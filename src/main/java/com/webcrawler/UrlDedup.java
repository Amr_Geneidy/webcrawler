package com.webcrawler;

public interface UrlDedup {
    boolean isDuplicate(String url);
}
